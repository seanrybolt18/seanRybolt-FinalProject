<%@ include file="/WEB-INF/layouts/include.jsp" %>


<style>
p{
	padding:15px;
}


</style>

<h1>Cryptocurrency Tracker</h1>
<div class="row">
	<div class="card">
		<p class="bg-light">This tracker is meant to help display some of the top Cryptocurrencies currently in circulation as well as give
		the user the ability to send themselves a text message to take note of current statistics of select currencies.
		The left input allows for the toggled options to be messaged to the user while the right button is
		 how many cryptocurrencies to display(Maximum of 500).</p>
		<div class="input-group col-10 m-3 mb-4">
		  <input id="phoneNumber" type="text" class="form-control" placeholder="Phone Number (10 Digits no Dashes)" aria-label="Recipient's username">
		  <div class="input-group-append">
		    <button class="btn btn-primary" id="sendBtn">Send</button>
		</div>
		<div class="input-group col-6" id="button">
		  <input id="coinCount" type="text" class="form-control" placeholder="# of Currencies" aria-label="Recipient's username" aria-describedby="basic-addon2">
		  <div class="input-group-append">
		    <button class="btn btn-success" id="submitBtn">Submit</button>
		</div>
</div>
</div>
	</div>
	
	
	
	<orly-table id="coinTable" loaddataoncreate includefilter maxrows="10" tabletitle="Cryptocurrencies" class="invisible"
	 url="">
		<orly-column field="action" label="Action" class="toggle">
			<div slot="cell">
				<orly-toggle name="action"></orly-toggle>
			</div>
		</orly-column>
		<orly-column field="id" label="Id" sorttype="natural"></orly-column>
		<orly-column field="name" label="Name"></orly-column>
		<orly-column field="symbol" label="Symbol"></orly-column>
		<orly-column field="rank" label="Rank"></orly-column>
		<orly-column field="type" label="Type"></orly-column>
	</orly-table>
</div>



<script>

var contextPath = '${pageContext.request.contextPath}';
 

	orly.ready.then(()=>{
		var previousToggledItems = [];
		let submitButton = orly.qid("submitBtn");
		let sendButton = orly.qid("sendBtn");
		submitButton.addEventListener("click", (e) =>{
			try{
				let coinCountButton = orly.qid("coinCount");
				let coinCount = coinCountButton.value;
				let isnum = /^\d+$/.test(coinCount);
				if(coinCount > 500){
					throw "coinCount Exceeded 500"
				}
				if(coinCount <0){
					throw "coinCount less than 0"
				}
				if(!isnum){
					throw "coinCount not a number"
				}
				fetch("<c:url value='/coins/retrieve/' />" + coinCount, {
			        method: "POST",
			        body: JSON.stringify(coinCount),
			        headers: {
			            "Content-Type": "application/json"
			        }
				}).then(function(response) {
				  	if (response.ok) {
				  		let jsonPromise = response.json(); // object from controller
				  		return jsonPromise;
				  	} else {
				  		throw new Error(getError(response));
				  	}
				}).then(function(Response) {						
					if (Response != null && Response != "undefined") {
						let messageType = "info";
						let message = Response.message;
						orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
						populateTableWithResponse(Response);
						
					} else {
						orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
					}
	
				})}
			catch (err) {
				if(err == "coinCount Exceeded 500"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Please Select a number that is less than 500"});
					sendButton.removeAttribute("disabled");
				}
				else if(err == "coinCount less than 0"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Please Select a number that is a positive number"});
					sendButton.removeAttribute("disabled");
				}
				else if(err == "coinCount not a number"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Please input a number"});
					sendButton.removeAttribute("disabled");
				}

				console.log("Runtime Error-> " + err);
			}
			});
		
		sendButton.addEventListener("click" , (e) =>{
			try{
					sendButton.setAttribute("disabled", "");
					let phoneNumberInput = orly.qid("phoneNumber");
					let phoneNumber = phoneNumberInput.value
					let isnum = /^\d+$/.test(phoneNumber);
					let startingVal = phoneNumber.startsWith("0");
					if(!isnum){
						throw "Phone number not all numbers"
					}
					if(startingVal){
						throw "Phone Number began with 0"
					}
					if(phoneNumber.length>10){
						throw "Phone Number Exceeded 10 Digits"
					}
					if(phoneNumber.length<10){
						throw "Phone Number Less Than 10 Digits"
					}
					let coinTable = orly.qid("coinTable");
					let actions = document.querySelectorAll("orly-toggle");
					let itemRanks = [];
					let toggledItems = [];
					let itemInfo = {
					};
					let i=0;
					for(i; i<actions.length; i++){
						if(actions[i].attributes.length == 2){
							let itemRow = actions[i].parentNode.parentNode.parentNode;
							toggledItems.push(itemRow.children[1].children[1].innerText);
							itemRanks.push(itemRow.children[4].children[1].innerText);
						}
					}
					if(JSON.stringify(toggledItems) == JSON.stringify(previousToggledItems)){
						throw "Please select new Item"
					}
					
					if(toggledItems.length == 0){
						throw "Please toggle an option to send"
					}

					itemInfo.coinIdList = toggledItems;
					itemInfo.coinRankList = itemRanks;
					
					fetch("<c:url value='/coins/sendMessage/' />" + phoneNumber,  {
				        method: "POST",
				        body: JSON.stringify(itemInfo),
				        headers: {
				            "Content-Type": "application/json"
				        }
					}).then(function(response) {
					  	if (response.ok) {
					  		let jsonPromise = response.json(); // object from controller
					  		return jsonPromise;
					  	} else {
					  		throw new Error(getError(response));
					  		sendButton.removeAttribute("disabled");
					  	}
					}).then(function(Response) {						
						if (Response != null && Response != "undefined") {
							let messageType = Response.messageType;
							let message = Response.message;
							orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});
							if(Response.messageType == "danger"){
								removeToggle(toggledItems)
							}
							else{
								previousToggleItems = JSON.parse(JSON.stringify(toggledItems));
								toggledSentItems(toggledItems);
							}
						} else {
							sendButton.removeAttribute("disbaled");
							orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"Unknown Response From The Server"});
						}
						
		
					})}
			catch (err) {
				
				if(err == "Phone Number Exceeded 10 Digits"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"The Phone Number supplied is greater than 10 Digits."});
					sendButton.removeAttribute("disabled");
				}
				else if(err == "Phone Number Less Than 10 Digits"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"The Phone Number supplied is less than 10 Digits."});
					sendButton.removeAttribute("disabled");
				}
				else if(err == "Phone Number began with 0"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"The Phone Number supplied cannot start with 0."});
					sendButton.removeAttribute("disabled");
					
				}
				else if(err == "Phone number not all numbers"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"The Phone Number supplied cannot contain non-numbers."});
					sendButton.removeAttribute("disabled");
					
				}
				else if(err == "Please toggle an option to send"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"No options were selected to be sent. Please select an option."});
					sendButton.removeAttribute("disabled");
					
				}
				else if(err == "Please select new Item"){
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:"No new options were selected to be sent. Please select a new option."});
					sendButton.removeAttribute("disabled");
					
				}
				console.log("Runtime Error-> " + err);
				
			}
			
		});
		
				function populateTableWithResponse(Response) {
					try {
						let coinList = Response.coinList;
						
						if (coinList != null && coinList != "undefined") {
							coinTable.data = coinList;
							for(let i=0; i<coinList.length; i++){
								if(coinList[i].smsSent == "Y"){
									console.log(i);
								}
							}
						} else {
							console.log("Unable to populate table with coin list");
						}	
					} catch (e) {
						console.log("Unable to populate table with coin list");	
					}
				}
				
				function toggledSentItems(toggledItems){
					let coinTable = orly.qid("coinTable");
					let actions = document.querySelectorAll("orly-toggle");
					for(let i =0; i<actions.length; i++){
						if(actions[i].attributes.length > 1){
							actions[i].setAttribute("disabled", "");
						}
					}
					sendButton.removeAttribute("disabled");
				}
				function removeToggle(toggledItems){
					let coinTable = orly.qid("coinTable");
					let actions = document.querySelectorAll("orly-toggle");
					for(let i =0; i<actions.length; i++){
						if(actions[i].attributes.length > 1){
							actions[i].removeAttribute("disabled");
						}
					}
					sendButton.removeAttribute("disabled");
				}

	
	
})



</script>