package com.oreillyauto.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.oreillyauto.domain.Coins.Coin;
import com.oreillyauto.domain.Coins.CoinData;
import com.oreillyauto.domain.Coins.DataResponse;
import com.oreillyauto.domain.Coins.Response;
import com.oreillyauto.service.FinalProjectService;
import com.oreillyauto.util.TwilioUtil;

@Controller
public class FinalProjectController {
    //Uses coinpaprika API that tracks crypto currency statistics and rankings
    private static Integer PROGRESS = 0;
    
    
    @Autowired
    FinalProjectService finalProjectService;
    
    @GetMapping(value= {"/coins/finalproject"})
    public String getLandingPage(HttpServletRequest req, Model model) throws JsonProcessingException{
        
        String url = "https://api.coinpaprika.com/v1/coins";
        
        Response coinResponse = new Response();
        HttpSession session = req.getSession();
        session.setAttribute("PROGRESS", 0);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //Adding user-agent to help avoid 403 error
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

        
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
        
        
        //Grabbing a list of coins
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<Coin>> response = restTemplate.exchange(url, HttpMethod.GET, entity, 
                                                                  new ParameterizedTypeReference<List<Coin>>() {});
        List<Coin> myCoins = response.getBody();
        
        //Saves top 500 coins/tokens
//        if(session.getAttribute("PROGRESS").toString() == "0")
          if(PROGRESS == 0){
            finalProjectService.saveDisplayList(myCoins);
            PROGRESS++;
//            session.setAttribute("PROGRESS", "1");
        }
        
        return "index";
    }
    

    @ResponseBody
    @PostMapping(value = {"/coins/retrieve/{coinCount}"})
    public Response getTestData(HttpServletRequest req, Model model, @PathVariable Integer coinCount) throws JsonProcessingException {
        Response coinResponse = new Response();
        List<Coin> dataBaseList = finalProjectService.getDbList(coinCount);
        try {
            coinResponse.setCoinList(dataBaseList);
            coinResponse.setMessage("Coin List added.");
        }
        catch(Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
        


        return coinResponse;
    }
    
    @ResponseBody
    @PostMapping(value = {"/coins/sendMessage/{phoneNumber}"})
    public Response sendMessage(HttpServletRequest req, Model model, @RequestBody DataResponse response, @PathVariable String phoneNumber ) throws JsonProcessingException {
        List<CoinData> messageInfo = new ArrayList<CoinData>();
        Response msgResponse = new Response();
        System.out.println(response.toString());
        List<String> coinIds = response.getCoinIdList();
        List<String> coinRanks = response.getCoinRankList();
        TwilioUtil practice = new TwilioUtil();
        phoneNumber = "+1" + phoneNumber;
        try {
        for(int i =0; i<coinIds.size(); i++) {
            //Do a similar process from the general Data get but this time for each toggled coin ID
            String url = "https://api.coinpaprika.com/v1/coins/" + coinIds.get(i) +"/ohlcv/latest/";
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            //Adding user-agent to help avoid 403 error
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");

            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity <List<CoinData>> apiResponse = restTemplate.exchange(url, HttpMethod.GET, entity, 
                                                                               new ParameterizedTypeReference<List<CoinData>>() {});
            messageInfo.add(apiResponse.getBody().get(0));
            System.out.println(messageInfo.get(i).toString());
            String myMessage = coinIds.get(i) + " " + messageInfo.get(i).toString();
            practice.sendSms(phoneNumber, myMessage, msgResponse);
        }
        
        }
        //Essentially catching unhandled exceptions
        catch(Exception e) {
            e.printStackTrace();
            msgResponse.setMessage("Unexpected error sending message to: " + phoneNumber);
        }

        //Handle a completed message
        if(msgResponse.isMessageStatus() == true) {
            msgResponse.setMessage("Sucessfully sent message to: " + phoneNumber);
            msgResponse.setMessageType("info");
        }
        //Handle unverified phone # failure
        else if(msgResponse.isMessageStatus() == false && msgResponse.getMessage() == "") {
            msgResponse.setMessage("Error sending message to: " +  phoneNumber + " number is not verified");
            msgResponse.setMessageType("danger");
        }
        //Handle land line error
        else if(msgResponse.isMessageStatus() == false && msgResponse.getMessage() != "") {
            msgResponse.setMessageType("danger");
        }
        finalProjectService.updateSMS(coinRanks);
        return msgResponse;
    }
    
    
    
    
}
