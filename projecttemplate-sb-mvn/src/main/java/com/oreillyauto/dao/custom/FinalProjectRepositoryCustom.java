package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Events.Event;

public interface FinalProjectRepositoryCustom {
    List<Event> getLimitedList(Integer coinCount);
    void updateSMS(List<Integer> coinRanks);
}
