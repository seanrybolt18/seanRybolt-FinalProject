package com.oreillyauto.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.FinalProjectRepositoryCustom;
import com.oreillyauto.domain.Events.Event;
import com.oreillyauto.domain.Events.QEvent;
import com.oreillyauto.domain.Events.QEventProperties;
import com.querydsl.jpa.impl.JPAUpdateClause;

@Repository
public class FinalProjectRepositoryImpl extends QuerydslRepositorySupport implements FinalProjectRepositoryCustom {


    private QEvent eventTable = QEvent.event;
    private QEventProperties propTable = QEventProperties.eventProperties;
    public FinalProjectRepositoryImpl() {
        super(Event.class);
    }

    @Override
    public List<Event> getLimitedList(Integer coinCount) {
        List<Event> dbEvents = (List<Event>)(Object) getQuerydsl().createQuery()
                .from(eventTable)
                .limit(coinCount)
                .fetch();
        
        return dbEvents;
    }

    @Override
    public void updateSMS(List<Integer> coinRanks) {
           long updatedCount = new JPAUpdateClause(getEntityManager(), eventTable)
                    .where(eventTable.eventId.in(coinRanks))
                    .set(eventTable.smsSent, "Y")
                    .execute();


        }
        
    }


