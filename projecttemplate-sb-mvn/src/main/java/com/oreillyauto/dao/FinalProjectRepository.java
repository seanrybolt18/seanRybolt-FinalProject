package com.oreillyauto.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.FinalProjectRepositoryCustom;
import com.oreillyauto.domain.Events.Event;

public interface FinalProjectRepository extends CrudRepository<Event, Integer>, FinalProjectRepositoryCustom {





}
