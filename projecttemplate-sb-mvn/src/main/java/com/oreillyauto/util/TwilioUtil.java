package com.oreillyauto.util;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.oreillyauto.domain.Coins.Response;
import com.twilio.Twilio;
import com.twilio.rest.lookups.v1.PhoneNumber;

public class TwilioUtil {
	private static final String MY_NUMBER = "+17139996933";
	private static final String ACCOUNT_SID = "ACe9d6b6778860de43a2e2f09004617a2b";
    private static final String AUTH_TOKEN = "3ab3f3f173527e13534e60a84a7e39cb";
    
	public TwilioUtil() {}
	    
	
	public void sendSms(String cellNumber, String message, Response msgResponse) {
		try {
		    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

	        
		    PhoneNumber phoneNumber = PhoneNumber.fetcher(
		                                                  new com.twilio.type.PhoneNumber("+15108675310"))
		                                              .setType(Arrays.asList("carrier")).fetch();
		    System.out.println(phoneNumber.getCarrier().toString());
	        if(phoneNumber.getCarrier().get("type") != "mobile") {
	            msgResponse.setMessage("Phone number supplied is not a Mobile/SMS capable phone number.");
	        }
//	           Alternate method to delivering message
//	           Message veriMessage = Message.creator(
//	                                                 new com.twilio.type.PhoneNumber(cellNumber),
//	                                                 new com.twilio.type.PhoneNumber(MY_NUMBER),
//	                                                 message)
//	                                             .create();
	           
			String url = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json";
	    	// Setup authentication and encode it
	    	String auth = ACCOUNT_SID + ":" + AUTH_TOKEN;
	    	byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
	    	// Create Request Headers
	    	HttpHeaders headers = new HttpHeaders();
	    	headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
	    	headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	    	headers.set("Authorization", "Basic " + new String(encodedAuth));
	    	// Create Request Body (Payload)
	    	MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
	    	params.add("To", cellNumber);
	    	params.add("From", MY_NUMBER);
	    	params.add("Body", message);
	    	// Send The Request to the Web Service and Print the Response
	    	HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(params, headers);
	    	RestTemplate restTemplate = new RestTemplate();
	    	ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
	    	msgResponse.setMessageStatus(true);	
	    	
		} catch (Exception e) {
		    e.printStackTrace();
		    System.out.println(e.getLocalizedMessage());
		    msgResponse.setMessageStatus(false);
		}
	}

	public static void main(String[] args) {

	}
}
