package com.oreillyauto.domain.Coins;

import java.util.List;

public class Response {
    
    List<Coin> coinList;
    List<String> coinIdList;
    List<String> coinRankList;
    String message;
    String messageType;
    boolean messageStatus;

    public Response() {};

    public List<Coin> getCoinList() {
        return coinList;
    }

    public void setCoinList(List<Coin> coinList) {
        this.coinList = coinList;
    }
    
    
    
    public List<String> getCoinRankList() {
        return coinRankList;
    }

    public void setCoinRankList(List<String> coinRankList) {
        this.coinRankList = coinRankList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    

    public List<String> getCoinIdList() {
        return coinIdList;
    }

    public void setCoinIdList(List<String> coinIdList) {
        this.coinIdList = coinIdList;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public boolean isMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(boolean messageStatus) {
        this.messageStatus = messageStatus;
    }
    
    
    }
