package com.oreillyauto.domain.Coins;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CoinData {
    
    private double high;
    private double open;
    private double low;
    private double close;
    private Timestamp time_open;
    private Timestamp time_close;
    
    CoinData(){}

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public Timestamp getTime_open() {
        return time_open;
    }

    public void setTime_open(Timestamp time_open) {
        this.time_open = time_open;
    }

    public Timestamp getTime_close() {
        return time_close;
    }

    public void setTime_close(Timestamp time_close) {
        this.time_close = time_close;
    }

    @Override
    public String toString() {
        return "Data high=" + high + ", open=" + open + ", low=" + low + ", close=" + close + ", time_open=" + time_open
                + ", time_close=" + time_close + "";
    }


      

}
