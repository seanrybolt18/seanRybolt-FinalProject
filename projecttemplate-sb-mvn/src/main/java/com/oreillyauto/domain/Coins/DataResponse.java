package com.oreillyauto.domain.Coins;

import java.util.List;

public class DataResponse {
    
    List<String> coinIdList;
    List<String> coinRankList;
    
    DataResponse(){}
    
    public List<String> getCoinIdList() {
        return coinIdList;
    }

    public void setCoinIdList(List<String> coinIdList) {
        this.coinIdList = coinIdList;
}
    
    

    public List<String> getCoinRankList() {
        return coinRankList;
    }

    public void setCoinRankList(List<String> coinRankList) {
        this.coinRankList = coinRankList;
    }

    @Override
    public String toString() {
        return "DataResponse [coinIdList=" + coinIdList + "]";
    }
    
    
}