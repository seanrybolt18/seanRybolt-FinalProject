package com.oreillyauto.domain.Coins;

public class Coin {

    private String id;
    private String name;
    private String symbol;
    private Integer rank;
    private String type;
    private String smsSent;
    
    public Coin(){};
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }

    @Override
    public String toString() {
        return "Coin [id=" + id + ", name=" + name + ", symbol=" + symbol + ", rank=" + rank 
                + ", type=" + type + "]";
    }


    

    

}
