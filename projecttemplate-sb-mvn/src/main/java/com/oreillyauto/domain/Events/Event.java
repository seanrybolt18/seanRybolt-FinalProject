package com.oreillyauto.domain.Events;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EVENTS")
public class Event implements Serializable{
    
    private static final long serialVersionUID = 1830388258183752175L;

    public Event() {};
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id", columnDefinition = "INTEGER")
    private Integer eventId;
    
    @Column(name="date_time", columnDefinition ="TIMESTAMP")
    private Timestamp dateTime;
    
    @Column(name="event_type", columnDefinition="VARCHAR(50)")
    private String eventType;
    
    @Column(name="sms_sent", columnDefinition="VARCHAR(1)")
    private String smsSent;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy ="event", cascade=CascadeType.ALL)
        private List<EventProperties> eventPropertiesList = new ArrayList<EventProperties>();

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }
    
    

    public List<EventProperties> getEventPropertiesList() {
        return eventPropertiesList;
    }

    public void setEventPropertiesList(List<EventProperties> eventPropertiesList) {
        this.eventPropertiesList = eventPropertiesList;
    }

    @Override
    public String toString() {
        return "Event [eventId=" + eventId + ", dateTime=" + dateTime + ", eventType=" + eventType + ", smsSent=" + smsSent + "]";
    }

    
    
}
