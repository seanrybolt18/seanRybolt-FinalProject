package com.oreillyauto.domain.Events;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="EVENT_PROPERTIES")
public class EventProperties implements Serializable{

    private static final long serialVersionUID = -3154000793192039134L;
    
    public EventProperties() {};
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "property_id", columnDefinition = "INTEGER")
    private Integer propertyId;
    
    
    @Column(name="event_key", columnDefinition ="VARCHAR(50)")
    private String eventKey;
    
    @Column(name="event_value", columnDefinition ="VARCHAR(160)")
    private String eventValue;
    
    @Column(name ="group_id", columnDefinition="VARCHAR(160)")
    private String groupId;
    
    @Column(name="sms_sent", columnDefinition="VARCHAR(1)")
    private String smsSent;
    
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "event_id", referencedColumnName = "event_id", columnDefinition = "INTEGER")
        private Event event;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }



    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public String getEventValue() {
        return eventValue;
    }

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(String smsSent) {
        this.smsSent = smsSent;
    }
    

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return "EventProperties [propertyId=" + propertyId + ", eventKey=" + eventKey + ", eventValue="
                + eventValue + ", groupId=" + groupId + ", smsSent=" + smsSent + "]";
    }
    
    
    

}
