package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//@Table(name="test")
public class Test implements Serializable {

	private static final long serialVersionUID = -4855603648459244422L;
	
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@Column(name = "ID", columnDefinition = "INTEGER")
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
