package com.oreillyauto.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.FinalProjectRepository;
import com.oreillyauto.domain.Coins.Coin;
import com.oreillyauto.domain.Events.Event;
import com.oreillyauto.domain.Events.EventProperties;
import com.oreillyauto.service.FinalProjectService;

@Service("finalProjectService")
public class FinalProjectServiceImpl implements FinalProjectService {
    
    @Autowired
    FinalProjectRepository finalProjectRepo;

    @Override
    public List<Coin> getDisplayList(List<Coin> myCoins, Integer coinCount) {
        List<Coin> displayList = new ArrayList<Coin>();
        for(int i=0; i<coinCount; i++) {
            displayList.add(i, myCoins.get(i));
        }
        return displayList;
    }

    @Override
    public void saveDisplayList(List<Coin> displayList) {
        for(int i=0; i<500; i++) {
            List<EventProperties> eventProperties = new ArrayList<EventProperties>();
            EventProperties currentProperty1 = new EventProperties();
            EventProperties currentProperty2 = new EventProperties();
            EventProperties currentProperty3 = new EventProperties();
            EventProperties currentProperty4 = new EventProperties();
            EventProperties currentProperty5 = new EventProperties();
            
            Event event = new Event();
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            event.setEventType("Coin");
            event.setDateTime(timestamp);
            

            Coin currentCoin = displayList.get(i);
            currentProperty1.setEventKey("COIN_ID");
            currentProperty1.setEventValue(currentCoin.getId());
            currentProperty1.setEvent(event);
            eventProperties.add(currentProperty1);
            
            currentProperty2.setEventKey("COIN_NAME");
            currentProperty2.setEventValue(currentCoin.getName());
            currentProperty2.setEvent(event);
            eventProperties.add(currentProperty2);
            
            currentProperty3.setEventKey("COIN_SYMBOL");
            currentProperty3.setEventValue(currentCoin.getSymbol());
            currentProperty3.setEvent(event);
            eventProperties.add(currentProperty3);
            
            currentProperty4.setEventKey("COIN_RANK");
            currentProperty4.setEventValue(currentCoin.getRank().toString());
            currentProperty4.setEvent(event);
            eventProperties.add(currentProperty4);
            
            currentProperty5.setEventKey("COIN_TYPE");
            currentProperty5.setEventValue(currentCoin.getType());
            currentProperty5.setEvent(event);
            eventProperties.add(currentProperty5);

            event.setEventPropertiesList(eventProperties);
            finalProjectRepo.save(event);
        }
        
    }

    @Override
    public List<Coin> getDbList(Integer coinCount) {
        List<Coin> DbList = new ArrayList<Coin>();
        List<Event> dbEvents = finalProjectRepo.getLimitedList(coinCount);
        for(int i=0; i<coinCount; i++) {
            Coin currentCoin = new Coin();
            Event currentEvent = dbEvents.get(i);
            List<EventProperties> eventProperties = currentEvent.getEventPropertiesList();
            
            currentCoin.setId(eventProperties.get(0).getEventValue());
            currentCoin.setName(eventProperties.get(1).getEventValue());
            currentCoin.setSymbol(eventProperties.get(2).getEventValue());
            currentCoin.setRank(Integer.parseInt(eventProperties.get(3).getEventValue()));
            currentCoin.setType(eventProperties.get(4).getEventValue());
            
            DbList.add(currentCoin);
        }
        
        return DbList;
    }
    
    @Override
    @Transactional
    public void updateSMS(List<String> coinRanksString) {
        List<Integer> coinRanks = new ArrayList<Integer>();
        for(int i=0; i<coinRanksString.size(); i++) {
            coinRanks.add(Integer.parseInt(coinRanksString.get(i)));
        }
        finalProjectRepo.updateSMS(coinRanks);
        
    }


}
