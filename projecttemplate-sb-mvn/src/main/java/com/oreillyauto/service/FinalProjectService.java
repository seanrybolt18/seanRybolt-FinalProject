package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Coins.Coin;

public interface FinalProjectService {

    List<Coin> getDisplayList(List<Coin> myCoins, Integer coinCount);
    void saveDisplayList(List<Coin> displayList);
    List<Coin> getDbList(Integer coinCount);
    void updateSMS(List<String> coinRanksString);

}
